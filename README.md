# Kamaro Engineering e.V. Footprint Libraries

This repository contains the official Kamaro Engineering e.V. footprint libraries.

**The libraries in this repository are intended to be used with KiCad version 6.**

Each footprint library is stored as a directory with the `.pretty` suffix. The footprint files are `.kicad_mod` files within.

Contribution guidelines can be found at http://kicad-pcb.org/libraries/contribute
The library convention can be found at http://kicad-pcb.org/libraries/klc/

Other KiCad library repositories are located:

* **Symbols:** https://gitlab.com/kamaro-engineering/pcbs/kamaro-packages3d
* **3D Models:** https://gitlab.com/kamaro-engineering/pcbs/kamaro-templates
* **Templates:** https://gitlab.com/kamaro-engineering/pcbs/kamaro-symbols

